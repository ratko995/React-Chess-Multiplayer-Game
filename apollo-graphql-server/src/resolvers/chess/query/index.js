import games from './games';
import game from './game';

const Query = {
  games,
  game
};

export default Query;

import playerOne from './playerOne';
import playerTwo from './playerTwo';

const Chess = {
  playerOne,
  playerTwo
};

export default Chess;
